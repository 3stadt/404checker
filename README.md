# Usage

- place a file called `links.csv` inside the same folder as the executable
- The first column must contain links, the file must be comma seperated
- when finished, a file `brokenLinks.csv` is created, containing all failed links + the error code or message 

When started with the parameter `-v` you'll get a progress shown on screen.

# Download

build yourself or [download the latest binary from gitlab](https://gitlab.com/3stadt/404checker/-/jobs/artifacts/master/browse/dist?job=build)
