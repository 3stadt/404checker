package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
)

var linkChan chan string
var brokenLinkChan chan string
var wg sync.WaitGroup

func init() {
	linkChan = make(chan string, 50)
	brokenLinkChan = make(chan string, 50)
	go checker()
	go collector()
}

func main() {
	fileName := "links.csv"
	lineCount, err := lineCounter(fileName)
	if err != nil {
		fmt.Printf("ERROR %s", err)
		os.Exit(1)
	}

	file, err := os.Open(fileName)
	if err != nil {
		fmt.Printf("ERROR %s", err)
		os.Exit(1)
	}
	defer file.Close()

	fmt.Printf("starting check for %d lines, this may take several minutes depending on the number of URLs to check...", lineCount)

	r := bufio.NewReader(file)
	i := 1
	for {
		url, _, err := r.ReadLine()
		if err == io.EOF {
			fmt.Println("\nFile reading is done.\nWaiting for remaining checks...")
			break
		} else if err != nil {
			fmt.Printf("ERROR %s", err)
			os.Exit(1)
		}
		urlString := strings.TrimSpace(string(url))
		urlString = strings.Split(urlString, ",")[0]
		if !strings.HasPrefix(urlString, "http") {
			continue
		}
		if len(os.Args) > 1 && os.Args[1] == "-v" {
			fmt.Printf("%d|", i)
		}
		wg.Add(1)
		linkChan <- urlString
		i++
	}
	wg.Wait()
	brokenCount, err := lineCounter("brokenLinks.csv")
	if err != nil {
		fmt.Printf("ERROR %s", err)
		os.Exit(1)
	}
	if brokenCount == 0 {
		fmt.Printf("Found no broken links\n")
		return
	}
	fmt.Printf("Found %d broken links\n", brokenCount-1)
}

func checker() {
	for {
		select {
		case url := <-linkChan:
			check(strings.TrimSpace(url))
		}
	}
}

func collector() {
	f, err := os.Create("brokenLinks.csv")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer f.Close()
	for {
		select {
		case url := <-brokenLinkChan:
			_, err := f.WriteString(fmt.Sprintf("%s\n", url))
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			wg.Done()
		}
	}
}

func check(url string) {
	r, err := http.Get(url)
	if err != nil || (r != nil && r.StatusCode != 200) {
		errStr := ""
		if err != nil {
			errStr = err.Error()
		}
		status := ""
		if r != nil {
			status = strconv.Itoa(r.StatusCode)
		}
		brokenLinkChan <- strings.TrimSuffix(fmt.Sprintf("%s,%s,%s", url, status, errStr), ",")
		return
	}
	wg.Done()
}

func lineCounter(fileName string) (int, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	r := bufio.NewReader(file)
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}
