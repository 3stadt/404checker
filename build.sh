#!/usr/bin/env sh

mkdir -p dist/windows
mkdir -p dist/linux
mkdir -p dist/mac

go mod tidy
GOOS=windows ARCH=amd64 go build -o dist/windows/404Checker.exe .
GOOS=linux ARCH=amd64 go build -o dist/linux/404Checker .
GOOS=darwin ARCH=amd64 go build -o dist/mac/404Checker .
